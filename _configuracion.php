<?php

	class Configuracion{

		public function __construct()
	    {
	 		session_start();   	
	    }

	    public function salir(){
	    	unset($_SESSION['login']);
	    	$array = array('mensaje'=>null,'redirect'=>'index.php');
	    	print_r(json_encode($array));
	    }

	    public function _verificar_login(){
	    	$array = array('mensaje'=>'Usuario y/o Contreña incorrecta','redirect'=>'index.php');
	    	if(isset($_SESSION['login']) && !empty($_SESSION['login']))
	    	{
	    		$_SESSION['login'] = true;
	    		$array = array('mensaje'=>'entrando','redirect'=>'bienvenido.php');
	        }
	        else{
	        	$array = $this->_logueo($_POST) ? array('mensaje'=>'entrando','redirect'=>'bienvenido.php') : array('mensaje'=>'error','redirect'=>'index.php');
	        }
	        print_r(json_encode($array));

	    }

	    private function _logueo($data=false)
	    {
	    	
	    	$salida = false;
	    	if($data){
	    		$usuario = $data['usuario'];
		    	$password = $data['password'];
		    	if($usuario == 'alfonso@ikigaisolutions.com' && $password == '1qaz2wsx') {
		    		$salida = true;
		    		$_SESSION['login'] = true;
		    	}	
	    	}
	    	return $salida;
	    }
	}

	$instancia = new Configuracion();

?>