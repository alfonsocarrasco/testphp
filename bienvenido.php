<?php 
session_start(); (isset($_SESSION['login'])? true : header('location: index.php ')); 
?>

<!DOCTYPE html>
<html>
    <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title> pagina </title>
    <link href="assets/bootstrap/css/bootstrap.css" rel="stylesheet" />
    <link href="assets/css/base.css" rel="stylesheet" />
    </head>
    <body>
        <div class="container fondo-blanco p-5">
            <div class="row">
                <div class="col">
                    Bienvenido, <div class="btn btn-danger" id="cerrar_sesion">cerrar</div>
                </div>
            </div>
        </div>
    </body>
    <script src="assets/js/app.js"></script>
    <script src="assets/js/bienvenido.js"></script>
</html>