function pagina(){
	
	this.inicio = function()
	{
		let btn_entrar = document.querySelector("#entrar");
		if(btn_entrar != undefined && btn_entrar != null){
			btn_entrar.onclick = function(){
				if(app.validarFormulario(frmlogin)){
					let usuario = document.querySelector("#user");
					let password = document.querySelector("#password");
					
					const formData = new FormData();
					formData.append("usuario",usuario.value);
					formData.append("password",password.value);
					
						app.fetch_('_inicio',formData);

				}
			}
		}
	}

}
var home = new pagina();
home.inicio();
