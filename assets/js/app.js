function aplicativo(){
	
	this.fetch_ = function(pagina, datos){
		let controlador = pagina+'.php';
		fetch(controlador, {
	        method: 'POST',
	        body: datos
	    }).then(response => {
	        return response.json();
	    }).then(data => {
	    	if(data.mensaje != null){ alert(data.mensaje); }
	        window.location.href = data.redirect;
	    });
	}

	this.formulario_datos = function(frm){
		let elements = document.forms.namedItem(frm).elements;
		if(elements != undefined && elements != null){
			const formData = new FormData();
			for (i=0; i<elements.length; i++){
				let elemento = elements[i];
				formData.append(elemento.name, elemento.value);
			}
			return formData;
		}
	}

	this.validarFormulario = function(frm){
		if(frm != undefined && frm != null){
			if(frm.checkValidity()){
				return frm.checkValidity();
			}
			else{
				frm.reportValidity();
				return false;
			}
		}
	}

}
var app = new aplicativo();
