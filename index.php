<!DOCTYPE html>
<html>
    <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title> pagina </title>
    <link href="assets/bootstrap/css/bootstrap.css" rel="stylesheet" />
    <link href="assets/css/base.css" rel="stylesheet" />
    </head>
    <body>
        <div class="container fondo-blanco p-5">
            <div class="row">
                <div class="col">
                    <form id="frm_login" name="frmlogin">
                        <div class="mb-3">
                          <label for="user" class="form-label">Email</label>
                          <input type="email" class="form-control" id="user" placeholder="ejemplo@pagina.com" required>
                        </div>
                        <div class="mb-3">
                          <label for="password" class="form-label">Password</label>
                          <input type="text" class="form-control" id="password" placeholder="****" required>
                        </div>
                        <div class="mb-3 text-center">
                            <div class="btn btn-primary" id="entrar">Entrar</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </body>
    <script src="assets/js/app.js"></script>
    <script src="assets/js/inicio.js"></script>
</html>